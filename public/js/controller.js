'use strict';

angular.module('myApp.home', ['ngRoute', 'ngMap'])

    // Route Configurations
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'js/home.html',
            controller: 'HomeCtrl',
            controllerAs: 'vm'
        });
    }])

    // service to get Tweets response from server
    .service("HomeService", ['$http','$cookieStore', function ($http,$cookieStore) {
        var vm = this;

        //get the markerList according to search text
        vm.getMarkerList = function (placeName) {

            return $http({
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded','identifier':$cookieStore.get('identifier')},
                url: 'api/v1/tweet/search/?city=' + placeName
            });
        };

        //get the search history list
        vm.getSearchHistoryList = function () {
            return $http({
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded','identifier':$cookieStore.get('identifier')},
                url: 'api/v1/tweet/history'
            });
        };
    }])

    //Controller
    .controller('HomeCtrl', ['NgMap', 'HomeService','$cookieStore', function (NgMap, HomeService,$cookieStore){
        var vm = this;
        vm.map = '';
        vm.tweets = [];
        vm.tweet = '';
        vm.placeName = '';
        vm.searchHistoryList = [];
        vm.showHistoryList = false;
        vm.mapCenter = [27.7172, 85.3240];
        vm.googleMapBounds = new google.maps.LatLngBounds();
        vm.tweetsMarker = '';
        vm.tweetResponse = true;
        vm.tweetLocationName ='';
        vm.serverResponse = false;
        vm.identifier ='';

        //get the map List
        NgMap.getMap().then(function (map) {
            vm.map = map;
        });

        // Render Tweets in the map
        vm.renderMarkerToMap = function (placeName) {

            vm.tweetResponse = true;
            vm.placeName = placeName;


            vm.tweetsMarker = HomeService.getMarkerList(placeName);
            if (vm.tweetsMarker) {

                vm.tweetsMarker.then(function (response) {

                    vm.serverResponse = true;

                    vm.identifier = (response.headers('identifier'));

                    if(vm.identifier !== '')
                    {
                        $cookieStore.put('identifier',vm.identifier);
                        console.log(vm.identifier);
                    }




                    var responseContent = response.data.data;
                    if(!responseContent.length){
                        vm.tweetResponse = false;
                        vm.noResponseSection = placeName;
                        return false;
                    };


                    vm.tweets = [];
                    var googleMapBounds = new google.maps.LatLngBounds();


                    angular.forEach(responseContent, function (value, key) {

                        var latLng = {lat: value.geoCode[1], lng: value.geoCode[0]};
                        googleMapBounds.extend(latLng);

                        var loc = {
                            id: key,
                            tweet: value.tweetText,
                            user_profile: value.userProfilePic,
                            created_at: value.date,
                            position: [value.geoCode[1], value.geoCode[0]]
                        };
                        this.push(loc);

                    }, vm.tweets);


                    if(vm.map){

                        vm.map.setCenter(googleMapBounds.getCenter());
                        vm.map.fitBounds(googleMapBounds);
                    }

                    vm.tweetLocationName = placeName;

                });


            }
            ;
        };


        vm.mapCallBack = function () {
            //callBack function for map
        };

        //on marker click
        vm.handleMarkerClick = function (event) {
            vm.map.showInfoWindow(1, this);
        };

        vm.showDetail = function (e,stweet) {
            vm.sTweet = stweet;
           vm.map.showInfoWindow('foo', stweet.id.toString());
        };

        vm.hideDetail = function () {
            vm.map.hideInfoWindow('foo-iw');
        };

        vm.handleSearchButtonClick = function () {
            //call the api according to the search text,
            // hide the history List
            vm.serverResponse = false;
            vm.showHistoryList = false;
            vm.tweets = [];
            vm.renderMarkerToMap(vm.placeName);

        };

        //handle reset button click
        vm.handleHistoryButtonClick = function () {
            //TODO call the api to get the history List
            vm.serverResponse = false;

            vm.searchHistoryList = [];
            HomeService.getSearchHistoryList().then(function (response) {
                vm.serverResponse = true;
                vm.identifier = (response.headers('identifier'));

                if(vm.identifier !== '')
                {
                    $cookieStore.put('identifier',vm.identifier);
                }


                //TODO assign to search History List
                if (response.data.data) {
                    vm.searchHistoryList = response.data.data;
                }
            });

            vm.showHistoryList = true;
        };


        vm.handleHistoryListClick = function (element) {
            vm.serverResponse = false;

            var placeName = element.name;
            vm.renderMarkerToMap(placeName);

        };


        vm.initializeMap = function () {
            vm.placeName = 'bangkok';
            vm.handleSearchButtonClick();

        };

        //initialize the data in the map.
        vm.initializeMap();

    }]);
