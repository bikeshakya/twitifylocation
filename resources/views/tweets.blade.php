<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <title>Twitify Location Info on Map</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
</head>

<body ng-app="myApp">
<div>
    <div ng-view>
    </div>
</div>


<script src="https://maps.googleapis.com/maps/api/js?key={{Config::get('tweetConfig')['map']['apiKey']}}"></script>
<script type='text/javascript' src="{{ asset('bower_components/angular/angular.js') }}"></script>
<script type='text/javascript' src="{{ asset('bower_components/angular-route/angular-route.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('bower_components/ngmap/build/scripts/ng-map.js') }}"></script>
<script type='text/javascript' src="{{ asset('bower_components/angular-cookies/angular-cookies.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/app1.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/controller.js') }}"></script>

</body>
</html>

