TwitifyLocation project is build using laravel framework, with the support of angular js at front end section.

To setup the project,

 1. git clone
 
 2. run command "composer install" with in directory for php dependencies 
 
 3. setup .env from .env.example
 
 4. setup database
 
 5. run command "php artisan migrate" to have tables setups
 
 6. from directory /public  run command "bower install" for front end dependencies
 
 7. run command "php artisan serve" to serve 
 

 