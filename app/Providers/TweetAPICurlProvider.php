<?php

namespace App\Providers;

use App\Services\ServiceInterfaces\TweetAPICurlInterface;
use App\Services\TwitterAPICurl;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class TweetAPICurlProvider extends ServiceProvider
{
    // protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register bindings in the container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TweetAPICurlInterface::class, function () {
            return new TwitterAPICurl();
        });
    }


}
