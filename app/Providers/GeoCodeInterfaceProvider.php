<?php

namespace App\Providers;

use App\Services\GeoCodeAPICurl;
use App\Services\ServiceInterfaces\ObtainGeoCodeInterface;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class GeoCodeInterfaceProvider extends ServiceProvider
{
    // protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register bindings in the container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ObtainGeoCodeInterface::class, function () {
            return new GeoCodeAPICurl();
        });
    }


}
