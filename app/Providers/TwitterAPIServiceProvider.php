<?php

namespace App\Providers;

use App\Services\GeoCodeAPICurl;
use App\Services\ServiceInterfaces\TwitterAPIServiceInterface;
use App\Services\TweetService;
use App\Services\TwitterAPICurl;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class TwitterAPIServiceProvider extends ServiceProvider
{
    // protected $defer = true;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register bindings in the container
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TwitterAPIServiceInterface::class, function () {

            return new TweetService(new TwitterAPICurl(), new GeoCodeAPICurl());
        });
    }


}
