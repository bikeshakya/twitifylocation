<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/7/17
 * Time: 7:35 AM
 */

namespace App;

use DateTime;

class Tweet{

    protected $tweetText;
    protected $date;
    protected $userName;
    protected $userProfilePic;
    protected $geoCode;

    /**
     * Set Tweets Data to object
     * Tweet constructor.
     * @param $tweetText
     * @param $date
     * @param $userName
     * @param $userProfilePic
     * @param $geoCode
     */
    public function __construct($tweetText,$date,$userName,$userProfilePic,$geoCode)
    {
        $this->tweetText = $tweetText;
        $this->setDate($date);
        $this->userName = $userName;
        $this->userProfilePic = $userProfilePic;
        $this->geoCode = $geoCode;
    }


    /**
     * Transform date while construction
     * @param mixed $date
     */
    private function setDate($date)
    {
        $date = new DateTime($date);
        $this->date = date_format($date, 'Y-m-d H:i:s');
    }


    /**
     * Return array of the object properties
     * @return array
     */
    public function getObjectArray()
    {
        return ['tweetText'=>$this->tweetText,
                'date'=>$this->date ,
                'userName' =>$this->userName,
                'userProfilePic'=>$this->userProfilePic,
                'geoCode'=>$this->geoCode
            ];
    }


}