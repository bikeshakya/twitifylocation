<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/6/17
 * Time: 12:47 PM
 */

namespace App\Services;
use App\History;
use App\Services\ServiceInterfaces\ObtainGeoCodeInterface;
use App\Services\ServiceInterfaces\TweetAPICurlInterface;
use App\Services\ServiceInterfaces\TwitterAPIServiceInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;


/**
 * Class TwitterAPIService
 * @package App\Services
 */
class TweetService implements TwitterAPIServiceInterface {

    protected $config;
    protected $baseUri;
    protected $debug;
    protected $api;
    protected $googleApi;
    protected $radiusLimit;

    /**
     * TweetService constructor.
     * @param TweetAPICurlInterface $api
     * @param ObtainGeoCodeInterface $googleApi
     */
    public function __construct(TweetAPICurlInterface $api, ObtainGeoCodeInterface $googleApi)
    {

        $tweetConfig = Config::get('tweetConfig');
        $twitterConfig = $tweetConfig['twitter'];
        $this->radiusLimit = $twitterConfig['searchRadii'];

        $this->api = $api;
        $this->googleApi = $googleApi;

    }


    /**
     * Get Tweets from the cache if available else from the twitter
     * @param $searchLocation
     * @param $identifier
     * @return array
     */
    public function getTweets($searchLocation,$identifier)
    {

       $historyLog = $this->storeToHistory($searchLocation,$identifier);

       if($historyLog){

           $cachedTweets = $this->getTweetsFromCache($searchLocation);
           if($cachedTweets)
           {
               return $cachedTweets;
           }

           $newTweets = $this->getTweetsFromTwitter($searchLocation);

           if($newTweets) {

               /** Cache the tweets for future use */
               ($this->cacheTweets($searchLocation, $newTweets));
           }

           return $newTweets;
       }

       return [];

    }


    private function storeToHistory($searchLocation,$identifier)
    {
        try{
            $history = History::where(['title'=>$searchLocation,'identifier'=>$identifier])->first();
            if($history){
                $history->searched_at = Carbon::now();
                $history->save();
            }
            else{
                $history = new History;
                $history->identifier = $identifier;
                $history->searched_at = Carbon::now();
                $history->title = $searchLocation;
                $history->save();
            }
            return true;
        }catch(\Exception $e){

            throw new \Exception($e->getMessage());

        }
    }


    private function cacheTweets($searchLocation, $newTweets)
    {
        try {
            $key = $searchLocation;
            $ttl = Config::get('tweetConfig')['cacheTime'];
            $expiresAt = Carbon::now()->addMinutes($ttl);
            return Cache::add($key, $newTweets, $expiresAt);
        }catch(\Exception $e){

            throw new \Exception($e->getMessage());

        }

    }


    /**
     * Get Tweets from Cache if saved
     * @param $searchLocation
     * @return array
     */
    private function getTweetsFromCache($searchLocation)
    {
        $cacheTweets = [];

        $key = $searchLocation;

        if(Cache::get($key))
        {
            $cacheTweets = Cache::get($key);
        }

        return  $cacheTweets;
    }

    /**
     * @param $searchLocation
     * @return array
     */
    private function getTweetsFromTwitter($searchLocation)
    {
        $searchQuery = [];
        $geoCode = $this->googleApi->getLatLng($searchLocation);

        if (null != $geoCode) {
            $searchQuery = [
                'q' => urlencode($searchLocation),
                'geocode' => sprintf('%s,%s,%s', $geoCode->lat, $geoCode->lng, $this->radiusLimit),
                'lang' => 'en',
            ];
        }

        $response = $this->api->getTweets($searchQuery);

        return $response;
    }


    public function getHistory($identifier)
    {
        try {
            $tweetHistory = History::where('identifier', $identifier)->orderBy('searched_at', 'desc')->pluck('title');
            $tweetList = [];
            foreach ($tweetHistory as $title) {
                $tweetList[] = ['name' => $title, 'url' => 'api/v1/tweet/search/?city=' . $title];
            }

            return $tweetList;
        }catch(\Exception $e){

            throw new \Exception($e->getMessage());

        }

    }
}
