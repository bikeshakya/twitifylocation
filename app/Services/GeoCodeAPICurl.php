<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/6/17
 * Time: 6:43 PM
 */

namespace App\Services;

use App\Services\ServiceInterfaces\ObtainGeoCodeInterface;
use Illuminate\Support\Facades\Config;

class GeoCodeAPICurl implements ObtainGeoCodeInterface
{

    protected $curlUrl;
    protected $apiKey;
    protected $sslVerifyHost;
    protected $curlTimeOut;



    public function __construct()
    {
        $tweetConfig = Config::get('tweetConfig');
        $this->curlUrl = $tweetConfig['map']['geoCodeEndPoint'];
        $this->apiKey = $tweetConfig['map']['apiKey'];
        $this->sslVerifyHost = $tweetConfig['sslVerifyHost'];
        $this->curlTimeOut = $tweetConfig['curlTimeOut'];
    }


    public function getLatLng($address)
    {

        try{

        $query = [
            'key' => $this->apiKey,
            'address' => urlencode($address),
        ];


        $url = $this->curlUrl . '?' . http_build_query($query);
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, $this->sslVerifyHost);
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->curlTimeOut);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $results = json_decode($response);
        curl_close($curl);


        $output = ($results->status == 'OK') ? $results->results[0]->geometry->location : null;
        return $output;
    }catch(\Exception $e){

        throw new \Exception($e->getMessage());

    }
    }
}