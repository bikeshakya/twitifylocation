<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/6/17
 * Time: 4:56 PM
 */

namespace App\Services;

use App\Services\ServiceInterfaces\TweetAPICurlInterface;
use App\Tweet;
use Illuminate\Support\Facades\Config;

class TwitterAPICurl implements TweetAPICurlInterface {

    protected $payload;
    protected $config;
    protected $accessToken;


    const CURL_TIMEOUT = 300;
    const SSLVERIFY_HOST = 2;


    protected $tokenEndPoint;
    protected $searchEndPoint;
    protected $twitterApiKeys;

    public function __construct()
    {

        $config = Config::get('tweetConfig')['twitter'];
        $this->tokenEndPoint = $config['tokenEndPoint'];
        $this->searchEndPoint = $config['searchEndPoint'];
        $this->twitterApiKeys = $config['twitterApiKeys'];

    }

    public function getTweets($searchQuery)
    {

        $formatTweets = [];

        $twitterResponse = $this->apiRequest($searchQuery);

        if(isset($twitterResponse->statuses)) {

            $tweets = $twitterResponse->statuses;

            if (0 === count($tweets)) {
                return [];
            }

            foreach ($tweets as $status) {

                if (isset($status->coordinates)) {

                    $st = new Tweet($status->text,$status->created_at, $status->user->screen_name,
                        $status->user->profile_image_url, $status->coordinates->coordinates);

                    $formatTweets[] = $st->getObjectArray();
                }
            }
        }

        return $formatTweets;

    }

    private function apiRequest($searchQuery)
    {

        try {
            $this->getBearerToken();
            $encodedAccessToken = $this->payload->access_token;

            $request_headers = [];
            $request_headers[] = "Authorization: Bearer {$encodedAccessToken}";

            $url = $this->searchEndPoint . '?' . urldecode(http_build_query($searchQuery));

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, self::SSLVERIFY_HOST);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
            curl_setopt($curl, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $result = curl_exec($curl);
            $response = json_decode($result);
            curl_close($curl);

            return ($this->search_results = !isset($response->errors) ? $response : null);
        }catch(\Exception $e){

            throw new \Exception($e->getMessage());

        }

    }


    private function getBearerToken()
    {
        /*
         * @var string Encode the keys as per the guideline provided in the twitter page.
         */
        $encodedKeys = $this->getEncodedConsumerKeyAndSecret();

        $request_headers = [];
        $request_headers[] = "Authorization: Basic {$encodedKeys}";

        /*
         * grant_type must be set to client_credentials
         *
         * @see https://dev.twitter.com/oauth/application-only
         */
        $post_data = 'grant_type=client_credentials';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->tokenEndPoint);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, self::SSLVERIFY_HOST);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($curl, CURLOPT_TIMEOUT, self::CURL_TIMEOUT);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        $this->payload =  json_decode($response);


        curl_close($curl);

        if ($this->payload === NULL) {
            throw new \Exception("Unknown Message Response from Twitter Received.");
        }

        // Successfully retrieved the twitter token
        if (!isset($this->payload->errors)) {
            if ($this->payload->token_type === 'bearer') {
                $this->accessToken = $this->payload->access_token;
            }
        }

    }

    private function getEncodedConsumerKeyAndSecret()
    {

        $encode = base64_encode(
            sprintf('%s:%s', $this->twitterApiKeys['consumerKey'], $this->twitterApiKeys['consumerSecret'])
        );

        return $encode;
    }
}