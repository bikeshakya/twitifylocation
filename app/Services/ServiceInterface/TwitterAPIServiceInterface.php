<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/6/17
 * Time: 1:05 PM
 */
namespace App\Services\ServiceInterfaces;

/**
 * Interface to get tweets and history data
 * Interface TwitterAPIServiceInterface
 * @package App\Services\ServiceInterfaces
 */
Interface TwitterAPIServiceInterface{

    /**
     * Get tweets as per searched location and identifier of a user
     * @param $searchLocation
     * @param $identifier
     * @return mixed
     */
    public function getTweets($searchLocation,$identifier);

    /**
     * Get user search history of tweets with identifier
     * @param $identifier
     * @return mixed
     */
    public function getHistory($identifier);

}