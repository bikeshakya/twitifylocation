<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/7/17
 * Time: 8:42 PM
 */

namespace App\Services\ServiceInterfaces;


Interface ObtainGeoCodeInterface
{

    /**
     * Obtain the geoCode Lat Lng from the address location
     * @param $address
     * @return mixed
     */
    public function getLatLng($address);
}