<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/7/17
 * Time: 8:38 PM
 */

namespace App\Services\ServiceInterfaces;

Interface TweetAPICurlInterface
{
    /**
     * Get the tweets with the provided search query
     * @param $searchQuery
     * @return mixed
     */
    public function getTweets($searchQuery);

}