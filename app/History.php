<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class History extends Model
{
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','identifier'
    ];

}
