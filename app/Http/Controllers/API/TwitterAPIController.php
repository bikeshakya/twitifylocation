<?php
/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 11/6/17
 * Time: 12:16 PM
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ServiceInterfaces\TwitterAPIServiceInterface;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class TwitterAPIController extends Controller
{

    protected $service;

    /**
     * TwitterAPIController constructor.
     * @param TwitterAPIServiceInterface $service
     */
    public function __construct(TwitterAPIServiceInterface $service)
    {
        $this->service = $service;
    }

    /**
     *  fetch tweets about the location as per keyword.
     *  Identifier in the header is used to identify the user as per local cache data
     * @param Request $request
     * @return string
     */
    public function fetchTweetsAboutLocation(Request $request)
    {


        $response = [];
        $identifier = $request->header('identifier');

        $location = $request->get('city');

        if($location!==''){

            $location = strtolower(preg_replace('/\s+/', '_', $location));
            $response = $this->service->getTweets($location,$identifier);

        }

        return response(json_encode(['data' => $response]));

    }


    /**
     *  Get the History of tweet search as per user defined by identifier keys.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHistoryOfTweets(Request $request)
    {

        $identifier = $request->header('identifier');

        $response = $this->service->getHistory($identifier);

        return response()->json(['data' => $response]);

    }

}