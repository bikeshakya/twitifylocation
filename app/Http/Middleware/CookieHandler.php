<?php

namespace App\Http\Middleware;

use Closure;
use DateTime;

class CookieHandler
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $identifier = $this->generateUserIdentifier();
        $headerIdentifier = $request->header('identifier');

        if(isset($headerIdentifier) && $headerIdentifier !== 'null')
        {
            $identifier = $headerIdentifier;
        }else{
            $request->headers->set('identifier',$identifier);
        }

        $response = $next($request);

        $response->header('identifier',$identifier);
        return $response;

    }


    private function generateUserIdentifier()
    {
        $date = new DateTime();
        return hash('sha256',$date->getTimestamp());
    }

}
